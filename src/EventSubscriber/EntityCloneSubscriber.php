<?php

namespace Drupal\entity_clone_inline_blocks\EventSubscriber;

use Drupal\entity_clone\Event\EntityCloneEvents;
use Drupal\entity_clone\Event\EntityCloneEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\layout_builder\Plugin\Block\InlineBlock;
use Drupal\layout_builder\SectionComponent;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Clone inline blocks during an entity clone.
 */
class EntityCloneSubscriber implements EventSubscriberInterface {

  /**
   * The UUID generation service.
   *
   * @var Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * The entity manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Stores the layout builder field values pre-clone.
   *
   * These are then processed post-clone in order to avoid the cloned entity
   * from even having a single revision where the inline blocks point back to
   * the original entity.
   *
   * @var array
   */
  protected $layoutFieldValues = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(UuidInterface $uuid_service, EntityTypeManagerInterface $entity_type_manager) {
    $this->uuidService = $uuid_service;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      EntityCloneEvents::PRE_CLONE => 'preClone',
      EntityCloneEvents::POST_CLONE => 'postClone',
    ];
  }

  /**
   * Add any layout field values to an internal cache and unset them.
   *
   * This is done in order to prevent any revision of the new entity from
   * having references back to the inline blocks of the original entity.
   */
  public function preClone(EntityCloneEvent $event) {
    // Check the on/off switch on the clone form.
    if (!$event->getProperties()['clone_inline_blocks']) {
      return;
    }

    $original_entity = $event->getEntity();
    $entity = $event->getClonedEntity();

    // Find any layout builder fields.
    if ($entity instanceof FieldableEntityInterface) {
      foreach ($entity->getFieldDefinitions() as $field_id => $field_definition) {
        if ($this->fieldIsLayout($field_definition)) {
          $field = clone $entity->get($field_id);
          $this->layoutFieldValues[$entity->getEntityTypeId() . ':' . $original_entity->id()][$field_id] = $field;
          unset($entity->{$field_id});
        }
      }
    }
  }

  /**
   * Duplicate any inline blocks.
   *
   * @param \Drupal\entity_clone\Event\EntityCloneEvent $event
   *   Clone event.
   */
  public function postClone(EntityCloneEvent $event) {
    // Check the on/off switch on the clone form.
    if (!$event->getProperties()['clone_inline_blocks']) {
      return;
    }

    $original_entity = $event->getEntity();
    $entity = $event->getClonedEntity();

    // Find any layout builder fields.
    if ($entity instanceof FieldableEntityInterface) {
      foreach ($entity->getFieldDefinitions() as $field_id => $field_definition) {
        if ($this->fieldIsLayout($field_definition)) {
          /** @var \Drupal\layout_builder\SectionListInterface $field */
          $field = $this->layoutFieldValues[$entity->getEntityTypeId() . ':' . $original_entity->id()][$field_id];
          $sections = $field->getSections();
          foreach ($sections as $delta => $section) {
            $components = $section->getComponents();
            foreach ($components as $uuid => $component) {
              $plugin = $component->getPlugin();
              if ($plugin instanceof InlineBlock) {
                // Duplicate the block, update the reference.
                $cloned_component = $this->cloneComponent($component);
                $sections[$delta]->insertAfterComponent($uuid, $cloned_component);
                $sections[$delta]->removeComponent($uuid);
              }
            }
          }
          $entity->set($field_id, $sections);
        }
      }
      $entity->save();
    }
  }

  /**
   * Is this a layout field?
   */
  protected function fieldIsLayout($field_definition) {
    return ($field_definition->getType() == 'layout_section');
  }

  /**
   * Clone a section component (that's an InlineBlock).
   */
  protected function cloneComponent($component) {
    $cloned_block = $this->cloneBlockContent($component->getPlugin());

    $configuration = $component->toArray()['configuration'];
    $additional = $component->toArray()['additional'] ?? [];
    // Support https://www.drupal.org/node/3180702
    if (isset($configuration['block_uuid'])) {
      $configuration['block_uuid'] = NULL;
    }
    $configuration['block_revision_id'] = NULL;
    // Clone referenced entities inside the block.
    $this->cloneReferencedEntities($cloned_block);

    // In order for block usage to be properly updated, layout builder handles
    // the actual saving.
    // @see \Drupal\layout_builder\InlineBlockEntityOperations::saveInlineBlockComponent()
    $configuration['block_serialized'] = serialize($cloned_block);
    $cloned_component = new SectionComponent($this->uuidService->generate(), $component->getRegion(), $configuration, $additional);
    return $cloned_component;
  }

  /**
   * Clone entity referenced entities.
   *
   * @param object $block
   *   The entity we like duplicate its ReferencedEntities.
   */
  protected function cloneReferencedEntities(&$block) {
    foreach ($block->getFieldDefinitions() as $field_id => $field_definition) {
      if ($field_definition->getType() === 'entity_reference_revisions') {
        $new_values = [];
        foreach ($block->{$field_id}->referencedEntities() as $delta => $entity) {
          $new_values[$delta]['entity'] = $entity->createDuplicate();
        }
        $block->set($field_id, $new_values);
      }
    }
    $block->save();
  }

  /**
   * Clone block content.
   *
   * This intentionally does not save the block, as layout builder handles that
   * and properly marks block usage.
   */
  protected function cloneBlockContent(InlineBlock $plugin_block) {
    // We have to use reflection for this as it's a protected method.
    $reflectionMethod = new \ReflectionMethod($plugin_block, 'getEntity');
    $reflectionMethod->setAccessible(TRUE);
    $entity = $reflectionMethod->invoke($plugin_block);
    return $entity->createDuplicate();
  }

}
