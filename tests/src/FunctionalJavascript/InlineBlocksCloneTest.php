<?php

namespace Drupal\Tests\entity_clone_inline_blocks\FunctionalJavascript;

use Drupal\layout_builder\Plugin\Block\InlineBlock;
use Drupal\layout_builder\SectionListInterface;
use Drupal\node\NodeInterface;
use Drupal\Tests\layout_builder\FunctionalJavascript\InlineBlockTestBase;

class InlineBlocksCloneTest extends InlineBlockTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_clone_inline_blocks',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  public function testEntityCloneInlineBlocks() {
    $this->drupalLogin($this->drupalCreateUser([
      'access contextual links',
      'configure any layout',
      'administer node display',
      'view all revisions',
      'access content',
      'create and edit custom blocks',
      'clone node entity',
    ]));
    $this->drupalGet(static::FIELD_UI_PREFIX . '/display/default');
    $this->submitForm(
      ['layout[enabled]' => TRUE, 'layout[allow_custom]' => TRUE],
      'Save'
    );

    // Add two blocks to the page and assert the content in each.
    $this->drupalGet('node/1/layout');
    $this->addInlineBlockToLayout('Block 1', 'Block 1 original');
    $this->addInlineBlockToLayout('Block 2', 'Block 2 original');
    $this->assertSaveLayout();
    $this->drupalGet('node/1');
    $this->assertSession()->pageTextContains('Block 1 original');
    $this->assertSession()->pageTextContains('Block 2 original');

    // Get the 2 inline block uuids.
    $node = \Drupal::entityTypeManager()->getStorage('node')->load(1);
    $uuids = $this->getUuids($node);
    $this->assertCount(2, $uuids);

    // Clone the node.
    $this->drupalGet('entity_clone/node/1');
    $this->submitForm(['clone_inline_blocks' => 1], 'Clone');

    // Edit cloned blocks.
    $block_1_locator = static::INLINE_BLOCK_LOCATOR;
    $block_2_locator = sprintf('%s + %s', static::INLINE_BLOCK_LOCATOR, static::INLINE_BLOCK_LOCATOR);
    $this->drupalGet('node/3/layout');
    $this->configureInlineBlock('Block 1 original', 'Block 1 cloned', $block_1_locator);
    $this->configureInlineBlock('Block 2 original', 'Block 2 cloned', $block_2_locator);
    $this->assertSaveLayout();

    // Verify edited content on cloned node.
    $this->drupalGet('node/3');
    $this->assertSession()->pageTextContains('Block 1 cloned');
    $this->assertSession()->pageTextContains('Block 2 cloned');

    // Verify the uuids are unique.
    $cloned_node = \Drupal::entityTypeManager()->getStorage('node')->load(3);
    $cloned_uuids = $this->getUuids($cloned_node);
    $this->assertCount(2, $cloned_uuids);
    $this->assertEmpty(array_intersect($uuids, $cloned_uuids));

    // Edit originals.
    $this->drupalGet('node/1/layout');
    $this->configureInlineBlock('Block 1 original', 'Block 1 edited', $block_1_locator);
    $this->configureInlineBlock('Block 2 original', 'Block 2 edited', $block_2_locator);
    $this->assertSaveLayout();

    $this->drupalGet('node/1');
    $this->assertSession()->pageTextContains('Block 1 edited');
    $this->assertSession()->pageTextContains('Block 2 edited');
  }

  /**
   * Get the inline block uuids from a given node's layout.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   *
   * @return array
   *   An array of inline block uuids from the layout.
   */
  protected function getUuids(NodeInterface $node):array {
    $uuids = [];
    /** @var \Drupal\layout_builder\SectionListInterface $sections */
    $sections = $node->get('layout_builder__layout');
    $this->assertInstanceOf(SectionListInterface::class, $sections);
    foreach ($sections->getSection(0)->getComponents() as $uuid => $component) {
      if ($component->getPlugin() instanceof InlineBlock) {
        $uuids[] = $uuid;
      }
    }
    return $uuids;
  }

}
