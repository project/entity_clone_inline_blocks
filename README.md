## Add Layout Block duplication when cloning nodes.

When using `entity_clone` to duplicate an entity with Layout Builder content, references to inline blocks are maintained. This is not the expected user experience since inline block content is usually considered one-off. In other words, if you copy a node with an inline block, you'd expect to get a copy of the inline block on the cloned node. A change to the block on the new node should not affect the block on the original page.

This module duplicates inline block content during the clone, and updates references on the new entity to the newly cloned blocks.

Limitations:
- Non-recursive: if inline blocks contain references to other entities, these are not currently duplicated.
